<!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />
<!--<link rel="stylesheet" href="dist/style.css" />-->

<script src="dist/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="dist/jquery-ui/jquery-ui.min.css" />
<link rel="stylesheet" href="game/style.css" />

</head>
<body>


<div id="page" style="background-color:#fff;">



	<div id="game1" class="game" style="background-image:url(game/bg_v1.png);">
		<div class="game_table" data-table="1"></div>
		<div class="game_table" data-table="2"></div>
	
		<?php 
		$itemATag = array();
		$itemATag[] = '<div class="game_item" data-answord="1" data-item="1"><img src="game/game1_item_1.png" alt="" /></div>';
		$itemATag[] = '<div class="game_item" data-answord="2" data-item="6"><img src="game/game1_item_6.png" alt="" /></div>';
		$itemATag[] = '<div class="game_item" data-answord="2" data-item="8"><img src="game/game1_item_8.png" alt="" /></div>';
		
		$itemA = array(0,1,2);
		$itemAsize = sizeof($itemA)-1;
		for($i=0; $i<$itemAsize; $i++)
		{
			$rand = rand(0,$itemAsize);
			$j = $itemA[$rand];
			$itemA[$rand] = $itemA[$i];
			$itemA[$i] = $j;
		}
		
		foreach($itemA as $item)
		{
			echo $itemATag[$item];
		}
		?>
	
		<?php 
		$itemBTag = array();
		$itemBTag[] = '<div class="game_item" data-answord="1" data-item="2"><img src="game/game1_item_2.png" alt="" /></div>';
		$itemBTag[] = '<div class="game_item" data-answord="1" data-item="3"><img src="game/game1_item_3.png" alt="" /></div>';
		$itemBTag[] = '<div class="game_item" data-answord="1" data-item="4"><img src="game/game1_item_4.png" alt="" /></div>';
		$itemBTag[] = '<div class="game_item" data-answord="1" data-item="5"><img src="game/game1_item_5.png" alt="" /></div>';
		$itemBTag[] = '<div class="game_item" data-answord="2" data-item="7"><img src="game/game1_item_7.png" alt="" /></div>';
		$itemBTag[] = '<div class="game_item" data-answord="2" data-item="9"><img src="game/game1_item_9.png" alt="" /></div>';
		
		$itemB = array(0,1,2,3,4,5);
		$itemBsize = sizeof($itemB)-1;
		for($i=0; $i<$itemBsize; $i++)
		{
			$rand = rand(0,$itemBsize);
			$j = $itemB[$rand];
			$itemB[$rand] = $itemB[$i];
			$itemB[$i] = $j;
		}
		
		foreach($itemB as $item)
		{
			echo $itemBTag[$item];
		}
		?>
		
		<div class="game_item_answord data-answord-1" style="left:-370px; top:-250px;"><img src="game/game1_item_1.png" alt="" /></div>
		<div class="game_item_answord data-answord-6" style="left:410px; top:-310px;"><img src="game/game1_item_6.png" alt="" /></div>
		<div class="game_item_answord data-answord-8" style="left:250px; top:-250px;"><img src="game/game1_item_8.png" alt="" /></div>
		<div class="game_item_answord data-answord-2" style="left:-40px; top:-310px;"><img src="game/game1_item_2.png" alt="" /></div>
		<div class="game_item_answord data-answord-3" style="left:-270px; top:-245px;"><img src="game/game1_item_3.png" alt="" /></div>
		<div class="game_item_answord data-answord-4" style="left:-260px; top:-245px;"><img src="game/game1_item_4.png" alt="" /></div>
		<div class="game_item_answord data-answord-5" style="left:-250px; top:-245px;"><img src="game/game1_item_5.png" alt="" /></div>
		<div class="game_item_answord data-answord-7" style="left:30px; top:-310px;"><img src="game/game1_item_7.png" alt="" /></div>
		<div class="game_item_answord data-answord-9" style="left:570px; top:-310px;"><img src="game/game1_item_9.png" alt="" /></div>
	</div>




</div>




<script>
var oldPos = [0,0];
var dontdropable = false;
var that;

// DRAGGABLE
$('.game_item').draggable({
	revert : function(event, ui) {
		returnpos(that);
	}
});
var stockArea = $('.game_table').droppable();
$( ".game_table" ).droppable({	
	drop: function( event, ui ) {
		if(that.data('answord') == $(this).data('table'))
		{
			that.css({opacity:0});
			$('.data-answord-' + that.data('item')).css({opacity:1});
		}
	}
});

// INPUT
$('.game_item').mousedown(function(){
	that = $(this);
	oldPos[0] = $(this).css('left');
	oldPos[1] = $(this).css('top');
	console.log( oldPos );
});
var returnpos = function(item){	
	item.animate({'left':oldPos[0], 'top':oldPos[1]}, 200);
}


$(".game_item_answord").mousedown(function(){return false});

</script>

</body>
</html>