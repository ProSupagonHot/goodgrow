<?php
$require = false;
if( isset($_POST['bookname']) )
{
	if( $_POST['bookname'] == '' )
	{
		// Return book name
		$require = "กรุณาใส่ชื่อหนังสือ";
	}else{
		// Create book
		$bookname = strtolower($_POST['bookname']);
		
		// Create Folder
		if ( !is_dir('books/' . $bookname) )
		{
			mkdir( 'books/' . $bookname );
			mkdir( 'books/' . $bookname . '/asset' );
			mkdir( 'books/' . $bookname . '/sourse' );
			
			rcopy('template/dist' , 'books/' . $bookname . '/dist');
			rcopy('template/layout' , 'books/' . $bookname . '/layout');

			header("Location: admin.php");
			die();
		}else{
			$require = "ชื่อหนังสือซ้ำ";
		}
	}
}

// Function to remove folders and files 
function rrmdir($dir) {
	if (is_dir($dir)) {
		$files = scandir($dir);
		foreach ($files as $file)
			if ($file != "." && $file != "..") rrmdir("$dir/$file");
		rmdir($dir);
	}
	else if (file_exists($dir)) unlink($dir);
}

// Function to Copy folders and files       
function rcopy($src, $dst) {
	if (file_exists ( $dst ))
		rrmdir ( $dst );
	if (is_dir ( $src )) {
		mkdir ( $dst );
		$files = scandir ( $src );
		foreach ( $files as $file )
			if ($file != "." && $file != "..")
				rcopy ( "$src/$file", "$dst/$file" );
	} else if (file_exists ( $src ))
		copy ( $src, $dst );
}

?><!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />

</head>
<body>

<div id="mainmenu">
	<a href="admin.php" title="">All books</a>
	<label><i class="glyphicon glyphicon-chevron-right"></i>Create book</label>
</div>

<div id="createbook">
	
	<form action="" method="POST">
		<div class="box">
			<strong>Book name*</strong> <?php if( $require ){ ?><label class="red"><?=$require?></label><?php } ?>
			<input type="text" name="bookname" class="form-control" />
			<label>*ห้ามใช้ . , - _ ' "</label>
		</div>
		
		<a href="index.php" class="btn btn-primary pull-right">Back</a>
		<button type="submit" class="btn btn-primary pull-right">Create</button>
	</form>
	
</div>




</script>

</body>
</html>