﻿<audio class="audio" controls preload="none"> 
   <source src="asset/lesson_1.mp3" type="audio/mpeg">
</audio>

<script>
// PRINT
$('.print').click(function(){
	window.print();
});


// AUDIO
$(".audio").trigger('play');
$(document).keypress(function(){
	$(".audio").trigger('pause');
	$(".audio").prop("currentTime", 0);
});
</script>

</body>
</html>