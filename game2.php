<!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />
<!--<link rel="stylesheet" href="dist/style.css" />-->

<script src="dist/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="dist/jquery-ui/jquery-ui.min.css" />
<link rel="stylesheet" href="game/style.css" />

</head>
<body>


<div id="page" style="background-color:#fff;">



	<div id="game2" class="game" style="background-image:url(game/game2_bg.png);">
		<div class="game_table" data-table="1"></div>
		<div class="game_table" data-table="2"></div>
	
		<?php 
				
		
		
		$itemB = array();
		$itemBTag = array();
		$itemBAnsword = array(1,2,2,2,1,1,1,2,1,2);
		$itemBsize = 9;
				
		for($i=0; $i<=$itemBsize; $i++)
		{
			$itemB[] = $i;
		}
		
		for($i=0; $i<$itemBsize; $i++)
		{
			$rand = rand(0,$itemBsize);
			$j = $itemB[$rand];
			$itemB[$rand] = $itemB[$i];
			$itemB[$i] = $j;
		}
		
		for($i=0; $i<=$itemBsize; $i++)
		{
			$itemBTag[] = '<div class="game_item bg' . $itemB[$i] . '" data-answord="' . $itemBAnsword[$i] . '" data-item="' .$i. '"><img src="game/game2_item_'.$i.'.png" alt="" /></div>';
		}
		
		foreach($itemB as $item)
		{
			echo $itemBTag[$item];
		}
		?>
		
		
		
		
		
		<div class="game_item_answord data-answord-0  bg<?=$itemB[0]?>"><img src="game/game2_item_0.png" alt="" /></div>
		<div class="game_item_answord data-answord-4  bg<?=$itemB[4]?>"><img src="game/game2_item_4.png" alt="" /></div>
		
		<div class="game_item_answord data-answord-1  bg<?=$itemB[1]?>"><img src="game/game2_item_1.png" alt="" /></div>		
		<div class="game_item_answord data-answord-2  bg<?=$itemB[2]?>"><img src="game/game2_item_2.png" alt="" /></div>
		<div class="game_item_answord data-answord-3  bg<?=$itemB[3]?>"><img src="game/game2_item_3.png" alt="" /></div>
		
		<div class="game_item_answord data-answord-5  bg<?=$itemB[5]?>"><img src="game/game2_item_5.png" alt="" /></div>
		<div class="game_item_answord data-answord-6  bg<?=$itemB[6]?>"><img src="game/game2_item_6.png" alt="" /></div>
		<div class="game_item_answord data-answord-8  bg<?=$itemB[8]?>"><img src="game/game2_item_8.png" alt="" /></div>
		
		<div class="game_item_answord data-answord-7  bg<?=$itemB[7]?>"><img src="game/game2_item_7.png" alt="" /></div>
		<div class="game_item_answord data-answord-9  bg<?=$itemB[9]?>"><img src="game/game2_item_9.png" alt="" /></div>
	</div>




</div>




<script>
var oldPos = [0,0];
var dontdropable = false;
var that;

// DRAGGABLE
$('.game_item').draggable({
	revert : function(event, ui) {
		returnpos(that);
	}
});
var stockArea = $('.game_table').droppable();
$( ".game_table" ).droppable({	
	drop: function( event, ui ) {
		if(that.data('answord') == $(this).data('table'))
		{
			that.css({opacity:0});
			$('.data-answord-' + that.data('item')).css({opacity:1});
		}
	}
});

// INPUT
$('.game_item').mousedown(function(){
	that = $(this);
	oldPos[0] = $(this).css('left');
	oldPos[1] = $(this).css('top');
	console.log( oldPos );
});
var returnpos = function(item){	
	item.animate({'left':oldPos[0], 'top':oldPos[1]}, 200);
}


$(".game_item_answord").mousedown(function(){return false});

</script>

</body>
</html>