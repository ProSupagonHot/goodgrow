<?php
$thisname = $_POST['thisname'];
$bookname = $_POST['bookname'];
$pagename = $_POST['pagename'];
$bgimage = $_POST['bgimage'];
$content = $_POST['content'];

$view = '
<?php include("layout/header.php"); ?>
<?php include("sourse/'.$pagename.'.php"); ?>
<?php include("layout/footer.php"); ?>
';
$viewfile = fopen("books/$bookname/$pagename.php", "w") or die("Unable to open file!");
fwrite($viewfile, $view);
fclose($viewfile);


$sourse = '
<div id="page" style="background-image:url('.substr($bgimage, 5, -2).');">
'.$content.'
</div>
';
$soursefile = fopen("books/$bookname/sourse/$pagename.php", "w") or die("Unable to open file!");
fwrite($soursefile, $sourse);
fclose($soursefile);

if( $thisname != $pagename )
{
	unlink("books/$bookname/sourse/$thisname.php");
	unlink("books/$bookname/$thisname.php");
}

header("Location: editor.php?book=".$_POST['bookname']."&page=".$pagename);
die();
?>