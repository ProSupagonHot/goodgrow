﻿<script>
// PRINT
$('.print').click(function(){
	window.print();
});


// AUDIO
$(".audio").trigger('load');
$('.audiobox').click(function(){
	if( $(this).attr('data-audio') == undefined || $(this).attr('data-audio') == 'pause' )
	{
		$(this).attr('data-audio', 'play');
		$(this).find('.audio').trigger('play');
	}
	else
	{
		$(this).attr('data-audio', 'pause');
		$(this).find('.audio').trigger('pause');
		$(".audio").prop("currentTime", 0);
	}
});
</script>
</body>
</html>