﻿
<script>
// PRINT
$('.print').click(function(){
	window.print();
});


// AUDIO
$(".audio").trigger('load');
$('.audiobox').click(function(){
	if( $(this).attr('data-audio') == undefined || $(this).attr('data-audio') == 'pause' )
	{
		$(this).attr('data-audio', 'play');
		$(this).find('.audio').trigger('play');
	}
	else
	{
		$(this).attr('data-audio', 'pause');
		$(this).find('.audio').trigger('pause');
		$(".audio").prop("currentTime", 0);
	}
});


// POPUP
$('.popupbtn').click(function(){
	popupshower($('.popup' + $(this).data('popup')));
});
$('.popup-background, .popup-close').click(function(){
	$('.popup-background').parent().find('.audio').trigger('pause');	
	popuphidden($(this).parent());
});

var popupshower = function(that){
	that.css({'opacity':0, 'display':'inline-block'}).animate({'opacity':1}, 300);
};
var popuphidden = function(that){
	that.animate({'opacity':0}, 300, function(){
		that.css({'display':'none'})
	});
};


// SLIDE
$('.slide-left').click(function(){
	if( $(this).css('opacity') == 1 )
	{
		$('.slide-button img').css('opacity', 0.9)
		var slider = $(this).parent().parent().find('.slider');
		var slider_size = slider.find(".slide-item").size() - 1;		
		var slider_current = parseInt( (parseInt(slider.css('left')) - 100) / 768);
		if( slider_current * -1 > 0 )
			slider.animate({'left': '+=100%' }, 1, function(){
				$('.slide-button img').css('opacity', 1);
			});
		else
			$('.slide-button img').css('opacity', 1);
	}
	$('.popup-background').parent().find('.audio').trigger('pause');
});
$('.slide-right').click(function(){
	if( $(this).css('opacity') == 1 )
	{
		$('.slide-button img').css('opacity', 0.9)
		var slider = $(this).parent().parent().find('.slider');
		var slider_size = slider.find(".slide-item").size() - 1;		
		var slider_current = parseInt( (parseInt(slider.css('left')) - 100) / 768);
		if( slider_size > slider_current * -1 )
			slider.animate({'left': '-=100%' }, 1, function(){
				$('.slide-button img').css('opacity', 1);
			});
		else
			$('.slide-button img').css('opacity', 1);
	}
	$('.popup-background').parent().find('.audio').trigger('pause');
});
</script>

</body>
</html>