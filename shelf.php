<!DOCTYPE html>
<html lang="en">
<head>
<title>โตไปไม่โกง</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />

<style>
body { width:100%; padding:0; margin:0; }
img { float:left; display:inline-block; margin:0; padding:0; border:none; }

#container { float:none; display:table; width:100%; max-width:1900px; margin:0 auto; }
.rower { float:left; display:inline-block; width:100%; }
#head { height:100vh; background-image:url('images/mu_head.png'); background-size:contain; background-position:50% 0%; }
#foot { height:100vh; background-image:url('images/mu_foot.png'); background-size:cover; background-position:50% 0%; }

#body { height:100vh; background-image:url('images/mu_body.png'); background-size:contain; background-position:50% 0%; }

#goepub { float:left; display:inline-block; width:100%; background-color:rgba(0,0,0,0); }
#goepub .epub { float:left; display:inline-block; width:5%; height:100px; background-color:rgba(0,0,0,0); }

</style>

</head>
<body>

<div id="container">
	<div class="rower">
		<img src="images/mu_head.png" width="100%" />
	</div>
	<div class="rower" style="overflow:hidden;">
		<img id="menu" src="images/mu_body.png" width="100%" />
		<div id="goepub">
			<div id="menu_row1" class="rower">
				<div class="epub" style="background-color:rgba(0,0,0,0.4); box-shadow:0 0 5px 0 #000;"></div>
				<div data-href="http://animation-genius.com/epub/view.php?book=_a2" data-image="mu_popup_1.png" class="epub modalshow"></div>
				<div class="epub"></div>
			</div>
			<div id="menu_row2" class="rower">
				<div class="epub" style="background-color:rgba(0,0,0,0.4); box-shadow:0 0 5px 0 #000;"></div>
				<div class="epub" style="background-color:rgba(0,0,0,0.4); box-shadow:0 0 5px 0 #000;"></div>
				<div data-href="http://animation-genius.com/epub/view.php?book=_p4" data-image="mu_popup_6.png" class="epub modalshow"></div>
			</div>
			<div id="menu_row3" class="rower">
				<div data-href="http://animation-genius.com/epub/view.php?book=_p5" data-image="mu_popup_7.png" class="epub modalshow"></div>
				<div data-href="http://animation-genius.com/epub/view.php?book=_p6" data-image="mu_popup_8.png" class="epub modalshow"></div>
			</div>
		</div>
	</div>
	<div id="foot" class="rower"></div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content" style="height:80vh; background-image:url('images/mu_popup.png'); background-position:50% 50%; background-size:contain; background-repeat:no-repeat; background-color:#f2cd7d;">
			
		</div>

	</div>
</div>
	
<script>
$(document).ready(function(){
	//alert( $('#menu').height() );
	var menuheight = $('#menu').height();
	$('#menu').parent().height( menuheight );
	$('#goepub').css({'height':menuheight+'px','margin-top':'-'+menuheight+'px'});
	
	$('#goepub .epub').css({'width':menuheight/8.7+'px', 'height':menuheight/7+'px', 'margin-right':menuheight/22+'px'});
	$('#menu_row1').css({'margin-top':menuheight/4.02+'px', 'margin-left':menuheight/2.93+'px'});
	$('#menu_row2').css({'margin-top':menuheight/22.5+'px', 'margin-left':menuheight/2.93+'px'});
	$('#menu_row3').css({'margin-top':menuheight/22.5+'px', 'margin-left':menuheight/2.93+'px'});
});

var href = '';
var image = '';
$('.modalshow').click(function(){
	href = $(this).attr('data-href');
	image = $(this).attr('data-image');

	$('.modal-content').css({'background-image':'url("images/'+image+'")'});
	$('#myModal').modal('show');
});

$('.modal-content').click(function(){
	window.location = href;
	
	$('#myModal').modal('hide');
});
</script>

</body>
</html>