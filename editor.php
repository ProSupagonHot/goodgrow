<?php 
if( !isset($_GET['book']) )
{
	header("Location: index.php");
}
?><!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="stylesheet" href="dist/font-awesome.min.css" />
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>

<link rel="stylesheet" href="dist/summernote/summernote-bs3.css" />
<link rel="stylesheet" href="dist/summernote/summernote.css" />
<script src="dist/summernote/summernote.min.js"></script>

</head>
<body>

<?php 
if( !isset($_GET['page']) ){ ?>
<div id="render"></div>
<?php }else{ ?>
<?php include 'books/'.$_GET['book'].'/sourse/'.$_GET['page'].'.php'; ?>
<script>
$('#page').attr('id', 'render');
</script>
<?php } ?>

<div id="editor">
	
	<div class="projectname">
		<strong>Book name:</strong><br />
		<input id="bookname" type="text" class="form-control" value="<?=(isset($_GET['book'])?$_GET['book']:'')?>" readonly />
	</div>
	
	<div class="projectname">
		<strong>Page name: *ชื่อของหน้ามีผลกับการเรียงลำดับหน้าหนังสือ</strong><br />
		<input id="pagename" type="text" class="form-control" value="<?=(isset($_GET['page'])?$_GET['page']:'')?>" />
	</div>
	
	<div class="box">
		<strong>Background image:</strong><br />
		<input id="imgInp" type="file" />
	</div>
	
	<div class="box">
		<form id="inserttext_form">
			<strong>Insert </strong>
			<select id="insertType">
				<option value="0">Image</option>
				<option value="1">Text</option>
				<option value="2">Audio</option>
			</select>
							
			<hr />
			<div id="insertImage">
				<input id="insertImageBrowse" type="file" /><br />
				<img id="insertImageShow" src="" width="50%" style="display:none;" />
			</div>
			<div id="insertText" style="display:none;">
				<textarea name="summernote" id="summernote"></textarea>
			</div>
			<div id="insertAudio" style="display:none;">
				<input type="text" value="" name="audio" class="form-control" />
			</div>
			
			<hr />						
			<div class="fullcol">
				<label>width</label>
				<input id="inserttext_width" type="number" value="" style="width:100px; margin:0 15px 0 2px;" placeholder="(px)" />
				<label>Height</label>
				<input id="inserttext_height" type="number" value="" style="width:100px; margin:0 15px 0 2px;" placeholder="(px)" />
			</div>
			<div class="fullcol">
				<label>pos x</label>
				<input id="inserttext_posx" type="number" value="" style="width:100px; margin:0 15px 0 2px;" placeholder="(px)" />
				<label>pos y</label>
				<input id="inserttext_posy" type="number" value="" style="width:100px; margin:0 15px 0 9px;" placeholder="(px)" />
			</div>
			<div class="fullcol" style="padding-top:10px; border-top:1px dashed #ccc;">
				<span id="inserttext_insert" class="btn btn-primary btn-xs" style="padding:3px 10px; margin:0 5px 0 0;">Insert</span>
				<input id="inserttext_reset" type="reset" value="Reset" class="btn btn-primary btn-xs" style="padding:3px 10px; margin:0 5px 0 0 ;" />
				<span id="inserttext_unselected" class="btn btn-warning btn-xs" style="padding:3px 10px; margin:0 5px 0 0; display:none;">Unselected</span>
				<span id="inserttext_remove" class="btn btn-danger btn-xs" style="padding:3px 10px; margin:0 5px 0 0; display:none;">Remove</span>
			</div>
		</form>
	</div>

	<button id="savefile" class="btn btn-primary">Save</button>
	<a href="admin.php<?=(isset($_GET['book'])?'?book='.$_GET['book']:'')?>" class="btn btn-primary">Back</a>	
	<?php if( isset($_GET['page']) ){ ?>
	<a href="books/<?=$_GET['book']?>/<?=$_GET['page']?>.php" class="btn btn-success" target="_blank">View</a>
	<a href="delete.php?book=<?=$_GET['book']?>&page=<?=$_GET['page']?>" class="btn btn-danger pull-right" onclick="return confirm('Are you sure?')">Delete</a>
	<?php } ?>
</div>

<form id="formeditor" action="generate.php" method="POST">
	<input type="hidden" name="thisname" value="" />
	<input type="hidden" name="bookname" value="" />
	<input type="hidden" name="pagename" value="" />
	<input type="hidden" name="bgimage" value="" />
	<input type="hidden" name="content" value="" />
</form>

<script>
/* SAVE BUTTON */
$('#savefile').click(function(){
	if( $('#pagename').val() != '' )
	{	
		if( insertbox != '' )
		{
			insertbox.css({'box-shadow':'none'});
			insertbox = '';
			
			$('#inserttext_insert').css({'display':'inline-block'});
			$('#inserttext_reset').css({'display':'inline-block'});
			$('#inserttext_unselected').css({'display':'none'});
			$('#inserttext_remove').css({'display':'none'});
			
			$('#inserttext_form')[0].reset();
			$('#summernote').summernote('code', '');
		}
		
		$('#formeditor input[name="thisname"]').val( '<?=(isset($_GET['page'])?$_GET['page']:'')?>' );
		$('#formeditor input[name="bookname"]').val( $('#bookname').val().toLowerCase() );
		$('#formeditor input[name="pagename"]').val( $('#pagename').val().toLowerCase() );
		$('#formeditor input[name="bgimage"]').val( $('#render').css('background-image') );
		$('#formeditor input[name="content"]').val( $('#render').html() );
		
		$('#formeditor').submit();
	}else{
		alert("please, insert page name นะ.");
	}
});




// EDIT INSERT BOX
var insertbox = '';
var drag = false;
var movediv = [0,0];
var movemouse = [0,0];
$('#render').delegate("div", "mousedown", function(){
	//alert( $(this).attr('class') );
	
	insertbox = $(this);
	$('#render div').css({'box-shadow':'none'});
	$(this).css({'box-shadow':'0 0 5px 0 #000'});
	
	movediv[0] = movemouse[0]-parseInt($(this).css('left'));
	movediv[1] = movemouse[1]-parseInt($(this).css('top'));
	
	if( $(this).hasClass( "audiobox" ) )
		$('#insertAudio input[name="audio"]').val( $(this).find('source').attr('src').substr(6, 1000) );
	else
		$('#insertAudio input[name="audio"]').val('');
		
	$('#summernote').summernote('code', $(this).html() );
	
	$('#inserttext_width').val( $(this).width() );
	$('#inserttext_height').val( $(this).height() );
	$('#inserttext_posx').val( parseInt($(this).css('left')) );
	$('#inserttext_posy').val( parseInt($(this).css('top')) );
	
	$('#inserttext_insert').css({'display':'none'});
	$('#inserttext_reset').css({'display':'none'});
	$('#inserttext_unselected').css({'display':'inline-block'});
	$('#inserttext_remove').css({'display':'inline-block'});
	
	drag = true;
});
$('#render').delegate("div", "mouseup", function(){
	drag = false;
});
$("#render").mousemove(function( event ) {
	//console.log(pageCoords);
	movemouse[0] = event.pageX;
	movemouse[1] = event.pageY;
	
	if( drag )
	{
		var divpos_x = event.pageX-movediv[0];
		if( divpos_x < 0 )
			divpos_x = 0;
			
		if( divpos_x > 757 )
			divpos_x = 757;
			
		var divpos_y = event.pageY-movediv[1];
		if( divpos_y < 0 )
			divpos_y = 0;
			
		if( divpos_y > 1013 )
			divpos_y = 1013;
			
		insertbox.css({'left':divpos_x, 'top':divpos_y});
		$('#inserttext_posx').val( divpos_x );
		$('#inserttext_posy').val( divpos_y );
	}
});
$('#inserttext_width').keyup(valuechange_width);
$('#inserttext_width').mousedown(valuechange_width);
function valuechange_width() {
	if( insertbox != '' )
	{
		insertbox.css({'width':$(this).val()+'px'});
	}
}
$('#inserttext_height').keyup(valuechange_height);
$('#inserttext_height').mousedown(valuechange_height);
function valuechange_height() {
	if( insertbox != '' )
	{
		insertbox.css({'height':$(this).val()+'px'});
	}
}
$('#inserttext_posx').keyup(valuechange_left);
$('#inserttext_posx').mousedown(valuechange_left);
function valuechange_left() {
	if( insertbox != '' )
	{
		insertbox.css({'left':$(this).val()+'px'});
	}
}
$('#inserttext_posy').keyup(valuechange_top);
$('#inserttext_posy').mousedown(valuechange_top);
function valuechange_top() {
	if( insertbox != '' )
	{
		insertbox.css({'top':$(this).val()+'px'});
	}
}
$('#inserttext_unselected').click(function(){			
	insertbox.css({'box-shadow':'none'});
	insertbox = '';
	
	$('#inserttext_insert').css({'display':'inline-block'});
	$('#inserttext_reset').css({'display':'inline-block'});
	$('#inserttext_unselected').css({'display':'none'});
	$('#inserttext_remove').css({'display':'none'});
	
	$('#inserttext_form')[0].reset();
	$('#summernote').summernote('code', '');
});
$('#inserttext_remove').click(function(){		
	insertbox.css({'box-shadow':'none'});
	insertbox.remove();
	insertbox = '';
	
	$('#inserttext_insert').css({'display':'inline-block'});
	$('#inserttext_reset').css({'display':'inline-block'});
	$('#inserttext_unselected').css({'display':'none'});
	$('#inserttext_remove').css({'display':'none'});
	
	$('#inserttext_form')[0].reset();
	$('#summernote').summernote('code', '');
});

// INSERT Audio
$('#insertAudio input[name="audio"]').change(function(){
	if( insertbox != '' )
	{
		insertbox.find('source').attr('src', 'asset/' + $(this).val());
	}
});

// INSERT FORM
$('#summernote').summernote({
	callbacks: {
		onChange: function(contents, $editable) {
			if( insertbox != '' )
			{	
				insertbox.html( contents );
				//console.log('onChange:', contents, $editable);			
			}
		}
	},
	height: 200,
    toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['view', ['codeview']],
    ]
});
var textbox = 0;
$('#inserttext_insert').click(function(e)
{
	e.preventDefault();
	
	if( inserttype == 0 )
	{
		var imgsrc = $('#insertImageShow').attr('src');
		if( imgsrc != '' )
		{
			if( $('#inserttext_width').val() == '' ){
				var width = 100;
			}else{
				var width = $('#inserttext_width').val();
			}
			
			if( $('#inserttext_height').val() == '' ){
				var height = 100;
			}else{
				var height = $('#inserttext_height').val();
			}
			
			if( $('#inserttext_posx').val() == '' ){
				var posx = 0;
			}else{
				var posx = $('#inserttext_posx').val();
			}
			
			if( $('#inserttext_posy').val() == '' ){
				var posy = 0;
			}else{
				var posy = $('#inserttext_posy').val();
			}
			
			$('#render').append('<div class="textbox' + textbox + '" style="width:' + width + 'px; height:' + height + 'px; left:' + posx + 'px; top:' + posy + 'px;">' + '<img src="' + imgsrc + '" width="100%" />' + '</div>');
			textbox++;
			
			$('#inserttext_form')[0].reset();
			$('#summernote').summernote('code', '');
		}
		else
		{
			alert("Please choose image");
		}
	}
	else if( inserttype == 1)
	{
		if( $('.note-editable').text() != '' )
		{
			if( $('#inserttext_width').val() == '' ){
				var width = 100;
			}else{
				var width = $('#inserttext_width').val();
			}
			
			if( $('#inserttext_height').val() == '' ){
				var height = 100;
			}else{
				var height = $('#inserttext_height').val();
			}
			
			if( $('#inserttext_posx').val() == '' ){
				var posx = 0;
			}else{
				var posx = $('#inserttext_posx').val();
			}
			
			if( $('#inserttext_posy').val() == '' ){
				var posy = 0;
			}else{
				var posy = $('#inserttext_posy').val();
			}
			
			$('#render').append('<div class="textbox' + textbox + '" style="width:' + width + 'px; height:' + height + 'px; left:' + posx + 'px; top:' + posy + 'px;">' + $('#summernote').summernote('code') + '</div>');
			textbox++;
			
			$('#inserttext_form')[0].reset();
			$('#summernote').summernote('code', '');
		}
		else
		{
			alert('please insert text label.');
		}
	}
	else if( inserttype == 2)
	{
		if( $('#insertAudio input[name="audio"]').val() != '' )
		{					
			if( $('#inserttext_width').val() == '' ){
				var width = 100;
			}else{
				var width = $('#inserttext_width').val();
			}
			
			if( $('#inserttext_height').val() == '' ){
				var height = 100;
			}else{
				var height = $('#inserttext_height').val();
			}
			
			if( $('#inserttext_posx').val() == '' ){
				var posx = 0;
			}else{
				var posx = $('#inserttext_posx').val();
			}
			
			if( $('#inserttext_posy').val() == '' ){
				var posy = 0;
			}else{
				var posy = $('#inserttext_posy').val();
			}
						
			$('#render').append('<div class="textbox' + textbox + ' audiobox" style="width:' + width + 'px; height:' + height + 'px; left:' + posx + 'px; top:' + posy + 'px;">' + '<audio class="audio" controls preload="none"><source src="asset/' + $('#insertAudio input[name="audio"]').val() + '" type="audio/mpeg"></audio>' + '</div>');
			textbox++;
			
			$('#inserttext_form')[0].reset();
			$('#summernote').summernote('code', '');
		}
		else
		{
			alert('please insert audio name.');
		}
	}
});
$('#inserttext_form').on('keyup keypress', function(e) {
	var code = e.keyCode || e.which;
	if (code == 13) { 
		e.preventDefault();
		return false;
	}
});
$('#inserttext_form button').click(function(e){
	e.preventDefault();
});


// LAYOUT
var window_width = $(window).width();
var window_height = $(window).height();
$('#editor').css({'width':window_width-808+'px'});


// BACKGROUND IMAGE
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#render').css({'background-image':'url('+e.target.result+')'});
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$("#imgInp").change(function(){
    readURL(this);
});


// INSERT TYPE
var inserttype = 0;
$('#insertType').change(function(){
	inserttype = $(this).val();
	
	$('#insertImage').css({'display':'none'});
	$('#insertText').css({'display':'none'});
	$('#insertAudio').css({'display':'none'});
	
	if( inserttype == 0 )
	{
		$('#insertImage').css({'display':'block'});		
	}
	else if( inserttype == 1)
	{
		$('#insertText').css({'display':'block'});	
	}
	else if( inserttype == 2)
	{
		$('#insertAudio').css({'display':'block'});	
	}
});





// INSERT TYPE IMAGE
function loadImage(input) 
{
    if (input.files && input.files[0]) 
	{
        var reader = new FileReader();

        reader.onload = function (e) 
		{
            $('#insertImageShow').attr('src', e.target.result);
            $('#insertImageShow').css({'display':''});
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$("#insertImageBrowse").change(function(){
    loadImage(this);
});
</script>

</body>
</html>