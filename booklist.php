<!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />

</head>
<body>



<div id="mainmenu">
	<a href="admin.php" title="">All books</a>
	<label><?=(isset($_GET['book']) ? '<i class="glyphicon glyphicon-chevron-right"></i> ' . $_GET['book'] : '')?></label>
</div>

<?php
$dir = "books" . (isset($_GET['book']) ? '/'.$_GET['book'] : '');
$files = array();
// Open a directory, and read its contents
if (is_dir($dir))
{
	if ($dh = opendir($dir))
	{
		while( ($file = readdir($dh)) !== false )
		{
			if( isset($_GET['book']) )
			{
				$file_explode = explode('.', $file);
				$file_type = array_pop($file_explode);
				if( $file_type == 'php' || $file_type == 'html' )
				{
					$files[] = $file_explode[0];
				}
			}else{
				$file_explode = explode('.', $file);
				if( sizeof($file_explode) == 1 )
				{
					//echo "filename:" . $file . "<br>";
					$files[] = $file;
				}
			}
		}
		closedir($dh);
	}
}
?>

<div id="booklistgen">

	<div class="row-fluid">
		<div class="col-xs-2 col-xs-offset-5">
			<input id="pluspage" type="number" name="" value="-7" class="form-control" />
		</div>
	</div>
	
	<table id="booklistgen_table" width="100%">
		<thead>
			<tr>
				<td width="50">Select</td>
				<td width="50">Page</td>
				<td width="25%">Name</td>
				<td>Title</td>
				<!--
				<td width="192">Preview</td>
				-->
			</tr>
		</thead>	
		<tbody>
			<?php foreach($files as $i=>$file){ ?>
			<tr>
				<td><input type="checkbox" name="booklistgen" value="<?=$i?>" /></td>
				<td data-page="<?=$i+1?>"><?=$i+1?></td>
				<td><?=$file?></td>
				<td>
					<input type="text" name="booklistgen[<?=$i?>]" class="form-control" />
				</td>
				<!--
				<td>
					<div>
						<iframe src="books/<?=$_GET['book']?>/<?=$file?>.php" width="768" height="1024" scrolling="no"></iframe>
					</div>
				</td>
				-->
			</tr>
			<?php } ?>
		</tbody>
	</table>

	<div class="row-fluid">
		<div class="col-xs-2 col-xs-offset-5">
			<span id="generate" class="btn btn-success" style="margin-top:10px;">Generate</span>
		</div>
	</div>

	<div id="booklistgen_print" class="row-fluid">
			asdl;asd;lma[s,d lmalsd'amsdlmaslmdlams;ldmasd
			asd 
			asd asd abss
			d abssd
			asd abssd as
	</div>
	
</div>

<script>
var pluspagechange = function(){
	var that = parseInt($('#pluspage').val());
	$('#booklistgen_table tbody tr td:nth-child(2)').each(function(){
		$(this).text(($(this).data('page') + that));
	});
};
$('#pluspage').change(pluspagechange);
pluspagechange();

$('#generate').click(function(){
	var data = '';
	$('#booklistgen_table tbody tr').each(function(){
		$('#booklistgen_print').css({'display':'table'}).text('');
		if( $(this).find('td:nth-child(1) input:checked').val() != undefined )
		{
			console.log( $(this).find('td:nth-child(1) input:checked').val() + ',' + $(this).find('td:nth-child(2)').text() + ',' + $(this).find('td:nth-child(3)').text() + ',' + $(this).find('td:nth-child(4) input').val() );
			var page = $(this).find('td:nth-child(2)').text();
			var name = $(this).find('td:nth-child(3)').text() + '.php';
			var title = $(this).find('td:nth-child(4) input').val();
			data += '<div style="float:left; display:inline-block; width:100%;">\
				<a href="' + name + '" title="">\
					<span style="float:left; display:inline-block;">' + title + '</span>\
					<span style="float:right; display:inline-block;">' + page + '</span>\
				</a>\
			</div>';
			$('#booklistgen_print').text(data);
		}
	});
});


</script>

</body>
</html>