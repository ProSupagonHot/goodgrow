<!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />

</head>
<body>



<div id="mainmenu">
	<a href="index.php" title="">All books</a>
	<label><i class="glyphicon glyphicon-chevron-right"></i></label>
	<label><a href="index.php?book=<?=$_GET['book']?>" title=""><?=(isset($_GET['book']) ? $_GET['book'] : '')?></a></label>
</div>

<?php
$dir = "books" . (isset($_GET['book']) ? '/'.$_GET['book'] : '');
$files = array();
// Open a directory, and read its contents
if (is_dir($dir))
{
	if ($dh = opendir($dir))
	{
		while( ($file = readdir($dh)) !== false )
		{
			if( isset($_GET['book']) )
			{
				$file_explode = explode('.', $file);
				$file_type = array_pop($file_explode);
				if( $file_type == 'php' || $file_type == 'html' )
				{
					$files[] = $file_explode[0];
				}
			}else{
				$file_explode = explode('.', $file);
				if( sizeof($file_explode) == 1 )
				{
					//echo "filename:" . $file . "<br>";
					$files[] = $file;
				}
			}
		}
		closedir($dh);
	}
}

sort($files, SORT_NATURAL | SORT_FLAG_CASE);
?>

<div id="booklist">
	
	<?php foreach($files as $i=>$file){ ?>
		<?php if( isset($_GET['book']) ) { ?>
		<div>
			<iframe src="books/<?=$_GET['book']?>/<?=$file?>.php" width="768" height="1024" scrolling="no"></iframe>
			<a href="view.php?book=<?=$_GET['book']?>&page=<?=$i?>"></a>
		</div>
		<?php }else{ ?>
		<a href="?book=<?=$file?>"><?=$file?></a>		
		<?php } ?>
	<?php } ?>
	
</div>

<script>
$('#booklist a').click(function(){
	location.reload();
});

$(document).ready(function(){
	var ifw = $('#booklist > div').width();
	$('#booklist > div').height( ifw*1.333 );
	
	var ifwr = ifw / 768;
	$('#booklist').find('iframe').css({'transform':'scale('+ifwr+', '+ifwr+')'});
});
$( window ).resize(function() {
	var ifw = $('#booklist > div').width();
	$('#booklist > div').height( ifw*1.333 );
	
	var ifwr = ifw / 768;
	$('#booklist').find('iframe').css({'transform':'scale('+ifwr+', '+ifwr+')'});
});

</script>

</body>
</html>