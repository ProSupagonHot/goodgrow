<?php 
if( !isset($_GET['book']) )
{
	header("Location: index.php");
}
?><!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB GENNERATER v.0.1</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />

</head>
<body>



<div id="mainmenu">
	<a href="index.php" title="">All books</a>
	<label><?=(isset($_GET['book']) ? '<i class="glyphicon glyphicon-chevron-right"></i>' . $_GET['book'] : '')?></label>
</div>

<?php
$dir = "books/".$_GET['book']."/asset";
$files = array();
// Open a directory, and read its contents
if (is_dir($dir))
{
	if ($dh = opendir($dir))
	{
		while( ($file = readdir($dh)) !== false )
		{
			if( $file != '.' && $file != '..' )
			{
				$files[] = $file;
			}			
		}
		closedir($dh);
	}
}
?>

<div id="booklist">
	
	<span class="icon" onClick="$('#upload').click();"><i class="glyphicon glyphicon-plus-sign"></i></span>
	
	<?php foreach($files as $file){ ?>
	<span style="background-image:url(books/<?=$_GET['book']?>/asset/<?=$file?>)"></span>
	<?php } ?>
	
	<input id="upload" type="file" multiple style="display:none;" />
	
</div>

<script>
$('#booklist a').click(function(){
	location.reload();
});

$('#upload').change(function(){
	alert("X");
});

</script>

</body>
</html>