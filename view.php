<!DOCTYPE html>
<html lang="en">
<head>
<title>EPUB VIEWER</title>

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<script src="dist/jquery.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/bootstrap.min.css" />
<link rel="stylesheet" href="style.css" />

<style>
body { overflow:hidden; }
</style>

</head>
<body>

<?php
$dir = "books/" . $_GET['book'];
$files = array();
// Open a directory, and read its contents
if (is_dir($dir))
{
	if ($dh = opendir($dir))
	{
		while( ($file = readdir($dh)) !== false )
		{
			$file_explose = explode('.', $file);
			if( isset($file_explose[1]) && $file_explose[1] == 'php' )
			{
				$files[] = $file;
				//echo $file.'<br />';
			}
		}
		closedir($dh);
	}
}

sort($files, SORT_NATURAL | SORT_FLAG_CASE);

if( !isset($_GET['page']) )
{
	$loadpage = 0;
}else{
	$loadpage = $_GET['page'];
}
?>

<div id="mainmenu">
	<a href="index.php" title="">All books</a>
	<label><i class="glyphicon glyphicon-chevron-right"></i></label>
	<label><a href="index.php?book=<?=$_GET['book']?>" title=""><?=$_GET['book']?></a></label>
	<label><i class="glyphicon glyphicon-chevron-right"></i> <span id="viewpage">1</span></label>
</div>

<div id="viewbook">
	<iframe src="books/<?=$_GET['book']?>/<?=isset($_GET['page'])?$files[$_GET['page']]:$files[0]?>" frameBorder="0" width="768" height="1024" scrolling="no"></iframe>
	
	<span id="btn-left"><i class="glyphicon glyphicon-chevron-left"></i></span>
	<span id="btn-right"><i class="glyphicon glyphicon-chevron-right"></i></span>
</div>



<script>
var windowWidth = $(window).width();
var windowHeight = $(window).height()-30;
var viewbookMargin;

$('#viewbook, #viewbook a').css({'height':windowHeight+'px'});
$('#viewbook a, #viewbook span').css({'line-height':windowHeight+'px', 'margin-top':'-1024px'});

var files = [];
<?php foreach( $files as $i=>$file ){ ?>
files[<?=$i?>] = '<?=$file?>';
<?php } ?>

var pagemax = <?=sizeof($files)?>;
var page = <?=isset($_GET['page'])?$_GET['page']:'0'?>;
$('#btn-left').click(function(){
	if( page > 0 )
	{
		page--;
		pager();
	}
});
$('#btn-right').click(function(){
	if( page < pagemax-1 )
	{
		page++;
		pager();
	}
});
function pager()
{
	$('#viewpage').text(page+1);
	$('#viewbook iframe').attr('src', 'books/<?=$_GET['book']?>/' + files[page]);
}









$(document).ready(function(){
	if( windowWidth*1.333 > windowHeight )
	{
		var scale = windowHeight / 1024;
		
		var vm = parseInt((windowWidth - (768 * scale)) / 2);
		
		viewbookMargin = '0 0 0 ' + vm + 'px';
	}
	else
	{
		var scale = windowWidth / 768;
		
		var vm = parseInt((windowHeight - (1025 * scale)) / 2);
		
		viewbookMargin = vm + 'px 0 0 0';
	}
	
	$('#viewbook iframe').css({'transform':'scale('+scale+', '+scale+')','transform-origin':'0 0','margin':viewbookMargin});
});
$( window ).resize(function() {
	if( windowWidth*1.333 > windowHeight )
	{
		var scale = windowHeight / 1024;
		
		var vm = parseInt((windowWidth - (768 * scale)) / 2);
		
		viewbookMargin = '0 0 0 ' + vm + 'px';
	}
	else
	{
		var scale = windowWidth / 768;
		
		var vm = parseInt((windowHeight - (1025 * scale)) / 2);
		
		viewbookMargin = vm + 'px 0 0 0';
	}
	
	$('#viewbook iframe').css({'transform':'scale('+scale+', '+scale+')','transform-origin':'0 0','margin':viewbookMargin});
});

</script>

</body>
</html>